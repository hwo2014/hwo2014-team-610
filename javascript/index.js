var net = require("net");
var JSONStream = require('JSONStream');
var DropTable = require('./DropTable/DropTable');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var trackName = process.argv[6] || 'keimola';

//console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort + " track: " + trackName);

client = net.connect(serverPort, serverHost, function() {

    return send({
        msgType: "join",
        data: {
            name: botName,
            key: botKey
        }
    });

/*

    return send({
        "msgType": "joinRace", 
        "data": {
              "botId": {
                "name": botName,
                "key": botKey
              },
              "trackName": "france",
              "password": "stalker",
              "carCount": 2
        }
    });
/*
return send({
    "msgType": "joinRace", 
    "data": {
          "botId": {
            "name": botName + "2",
            "key": botKey
          },
          "trackName": "france",
          "password": "salasana",
          "carCount": 2
    }
});
*/
});

var dt = new DropTable(botName, client);

function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
}

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {

    if (data.msgType === 'carPositions') {
        dt.tick(data);
        send({
            msgType: "throttle",
            data: dt.getThrottle()
        });

    } else {

        if (data.msgType === 'join') {
            //console.log('Joined');
        }
        else if (data.msgType === 'gameInit') {
            //console.log("Init");
            dt.init(data);
        }
        else if (data.msgType === 'gameStart') {
            //console.log('Race started');
            dt.start(data);
        }
        else if (data.msgType === 'gameEnd') {
            //console.log('Race ended');
        }
        else if (data.msgType === 'crash') {
            //console.log("CRASH", data.data);
            dt.crash(data);
        }
        else if (data.msgType === 'lapFinished') {
            dt.lapFinished(data);
            //console.log("LAP", data);
        }
        else if (data.msgType === 'turboAvailable') {
            dt.turboAvailable(data);
        }
        else {
            //console.log("something else");
            //console.log(data);
        }

        send({
            msgType: "ping",
            data: {}
        });
    }
});

jsonStream.on('error', function() {
    return console.log("disconnected");
});
