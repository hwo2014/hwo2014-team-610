var _ = require('underscore');

var Save = function() {
    var self = this;
    var tmp = {};
    var corners = [];

    this.lowestCrashAngle = 60;

    this.init = function() {
        fillWithDefaults();
    };

    this.getCornerWithRadius = function(radius){
        return _.find(corners, function(corner){
            return corner.radius == radius;
        });
    };

    this.updateData = function(pieceIndex, length, radius, speed, angle){
        if(pieceIndex != tmp.pieceIndex) {
            updateSave();
        }

        if(!tmp.speeds) tmp.speeds = [];
        tmp.speeds.push(speed);

        tmp.pieceIndex = pieceIndex;
        tmp.radius = radius;
        tmp.length = length;
        //tmp.highestSpeed = Math.max(tmp.highestSpeed, speed) || 0;
        tmp.highestAngle = Math.max(tmp.highestAngle, Math.abs(angle)) || 0;
    };

    this.crash = function() {
        tmp.crashed = true;
    };

    var updateSave = function() {
        var currentSave = self.getCornerWithRadius(tmp.radius);

        if(!currentSave) {
            addNewEntry();
            currentSave = self.getCornerWithRadius(tmp.radius);
        }

        cleanSpeeds();

        //console.log("Saved", currentSave);
        if(tmp.crashed === true) {
            updateCrashData(currentSave);
        } else {
            updateNonCrashData(currentSave);
        }

        tmp = {};
    };

    var updateNonCrashData = function(currentSave) {
        // If speed is more than current saved noncrash data.
        // Save it
        if(tmp.highestSpeed > currentSave.fastestSpeedWithoutCrash.speed) {
            currentSave.fastestSpeedWithoutCrash = {
                speed: tmp.highestSpeed,
                angle: tmp.highestAngle
            };
        }
    };

    var updateCrashData = function(currentSave) {
        this.lowestCrashAngle = Math.min(this.lowestCrashAngle, tmp.highestAngle);

        // If speed is less than current saved crash data.
        // Save it
        if(tmp.highestSpeed < currentSave.slowestSpeedWithCrash.speed) {
            currentSave.slowestSpeedWithCrash = {
                speed: tmp.highestSpeed,
                angle: tmp.highestAngle
            };
        }
    };

    var addNewEntry = function() {
        corners.push({
            radius: tmp.radius,
            length: tmp.length,
            fastestSpeedWithoutCrash: {
                speed: 0,
                angle: 0
            },
            slowestSpeedWithCrash: {
                speed: 9999,
                angle: 0
            }
        });
    };

    var cleanSpeeds = function() {
        if(!tmp.speeds) return;
        // Sort numbers so, that highest is in the end
        tmp.speeds.sort(function(a,b) { return a - b; });

        // Remove two of the biggest values
        // There's some weird spikes on the beginning of new piece
        // and we need to get rid of them
        tmp.speeds = tmp.speeds.slice(0, tmp.speeds.length-3);
        tmp.highestSpeed = tmp.speeds[tmp.speeds.length-1];
    };

    var fillWithDefaults = function() {
        // Placeholder
        // This would be the place where all of the pre-calculated
        // values would be stored in the beginning
    };

};

module.exports = Save;
