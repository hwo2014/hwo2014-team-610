var Util = require('./Util');
var PID = require('./PID');


var Car = function() {
    var self = this;

    // Comes from the server on init
    this.id = {};
    this.dimensions = {};

    this.speedDelta = 0;
    this.speed = 0;
	this.angleSpeed = 0;
    this.angle = 0;
    this.crashed = false;
    this.crashes = 0;
    this.throttle = 0.2;

    this.targetSpeed = -1;
    this.stableSpeedCooling = 0;

    this.currentPieceIndex = 0;
    this.currentPieceDistance = 0;
    this.currentPiece = {};
    this.nextPiece = {};

    this.lapDistance = 0;
    this.copySpeedMode = 0;
    this.lane = 0;

    this.currentLane = -1;
    this.nextLane = -1;
    this.isSwitchingLanes = false;

    this._state = ""; //
    this._states = {
        acceleration: "Acceleration",
        deceleration: "Deceleration",
        steadySpeed: "SteadySpeed"
    };

    this.ticks = 0;

    // These P, I, D variables will determine how the cruise control performs
    this.PID = new PID(5, 0, 0);

    this.getThrottle = function() {
        return this.throttle;
    }

    this.updateSpeed = function(distance, ticks) {
        // Ugly hack
        if( distance < 0 ) {
            return;
        }
        self.speedDelta = distance/ticks - self.speed;
        self.speed = distance/ticks;
    };

	this.updateAngleSpeed = function(angleDelta, ticks) {
		self.angleSpeed = angleDelta/ticks;
	};

    this.setAccelerateOn = function() {
        this._state = this._states.acceleration;
        this.throttle = 1.0;
        this.targetSpeed = 0;
    };
    this.setDecelerateOn = function() {
        this._state = this._states.deceleration;
        this.throttle = 0;
        this.targetSpeed = 0;
    };

    this.crash = function() {
        ++this.crashes;
    };

    this.tick = function() {
        if( this._state === this._states.steadySpeed && !Number.isNaN(this.speed) ) {
            this.throttle = Math.min(Math.max(this.PID.update(this.speed), 0),1);
        }
        if( this.ticks === 0 ) {
            console.log("Tick, Throttle, Speed, TargetSpeed, Angle, AngleSpeed, Crashes, CopySpeed, Lane");
        }
        console.log(this.ticks + "," + this.throttle.toFixed(2) + "," + this.speed.toFixed(2) + "," + this.targetSpeed.toFixed(2) + "," + this.angle.toFixed(2) + "," + this.angleSpeed.toFixed(2) + "," + this.crashes.toFixed(0) + "," + this.copySpeedMode.toFixed(0) + "," + this.currentLane.toFixed(0));
        ++this.ticks;
    };

    this.setSteadySpeedOn = function(speed) {
        this._state = this._states.steadySpeed;
        this.PID.setTarget(speed);
        this.targetSpeed = speed;
    };

};

module.exports = Car;
