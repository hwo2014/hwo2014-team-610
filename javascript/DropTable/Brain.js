var Car = require('./Car');
var Track = require('./Track');
var Util = require('./Util');
var Save = require('./Save');

// This is the 'kartturi' of the game
var Brain = function(data, name, parent) {

    var self = this;

    var root = parent;
    var initData = data;

    var currentTick = 0;
    var tickDelta = 0;
    var cars = [];

    var track;
    var playerName = name;
    var myColor = "";

    var save;

    // TODO: move elsewhere?
    var frictionMin = 0.0001;
    var frictionMax = 1;
    var gravity = 10;
    var mass = 1;
    var pieceMaxAngle = -1;
    var criticalAngle = 40;
    var pieceStartSpeed = -1;
    var currentPiece = -1;
    var currentLane = 0;

    var splineInitMaxSpeed = 40;
    var splineInitMinSpeed = 3;
    var splineMaxSpeed = splineInitMaxSpeed;
    var splineMinSpeed = Math.max(Math.random() * 3 * splineInitMinSpeed, splineInitMinSpeed);
    var splineMaxSpeedUpdated = 0;

    var lap = 0;
    var lapPosition = 0;
    var crashedLap = false;

    // Modes: ownSpeed, copyLap
    var ownSpeedMode = 0;
    var copyLapMode = 1;
    var controlMode = ownSpeedMode;

    var otherCars = {};
    var fastestLapCar = "";
    var carToFollow = "";

    var turboAvailable = false;

    this.init = function() {
        var carList = initData.data.race.cars;
        for (var i = 0; i < carList.length; i++) {
            var car = new Car();

            car.id = carList[i].id;
            car.dimensions = carList[i].dimensions;
            cars[car.id.name] = car;
            if( car.id.name !== playerName ) {
                otherCars[car.id.name] = { lapSpeeds: [], lap: 0, lapStarted: true, bestLapTime: 9999999999 };
            }
        }
        track = new Track(initData.data.race.track);
        track.init();

        save = new Save();
        save.init();
    };

    this.getThrottle = function() {
        return cars[playerName].getThrottle();
    };

    this.tick = function(data) {
        tickDelta = Math.abs(data.gameTick - currentTick);
        currentTick = data.gameTick;
        updateCars(data);
    };

    this.crash = function(data) {
        console.log("# A car crashed" , data.data.name);
        if( data.data.name === playerName ) {
	    console.log("Our crash");
            this.crashedLap = true;
            splineMaxSpeed *= 0.7;
            console.log("# Crash at speed " + car.speed + " / " + car.targetSpeed);
            updateFrictionEstimate(track.getBendRadius(currentPiece, currentLane), pieceStartSpeed, true);
            pieceStartSpeed = -1;
            //OUTLOGUtil.log(data);
            save.crash();
            /*
            var correction = getBinSearchCorrection();
            splineMaxSpeed -= correction;
            console.log("# Spline max speed updated to " + splineMaxSpeed);
            */
            car.crash();
        }
    	else {
    	    console.log("Other car crash", data.data.name);
    	}
    };

    this.lapFinished = function(data) {
        if( data.data.car.name === playerName ) {
            console.log("# Lap finished with crash", this.crashedLap);
            if( !this.crashedLap ) {
                this.splineMinSpeed *= 2;
            }
            this.crashedLap = false;
            ++this.lap;
            // After first lap, set controlMode to copy
            
            var fastestCar = null;
            var fastestLap = -1;
            console.log("Searching for fastest car");
            for( var c in otherCars ) {
                console.log("\t car lap", otherCars[c].bestLapTime);
                if( fastestLap === -1 || fastestLap > otherCars[c].bestLapTime ) {
                    fastestLap = otherCars[c].bestLapTime;
                    fastestCar = c;
                }
            }
            console.log("Found fastest car ", fastestCar, fastestLap);
            if( fastestCar !== null && fastestCar !== playerName ) {
                controlMode = copyLapMode;
                carToFollow = fastestCar;
                console.log("Set car to follow " + carToFollow);
            }
        }
        else {
            otherCars[data.data.car.name].lap += 1;
            otherCars[data.data.car.name].lapStarted = true;
            otherCars[data.data.car.name].fastestLap = data.data.ranking.fastestLap;
            otherCars[data.data.car.name].bestLapTime = otherCars[data.data.car.name].bestLapTime < data.data.lapTime.millis ? otherCars[data.data.car.name].bestLapTime: data.data.lapTime.millis;
        }
    };

    var getBinSearchCorrection = function() {
        var correction = splineInitMaxSpeed;
        ++splineMaxSpeedUpdated;
        for( var i = 0; i < splineMaxSpeedUpdated; ++i ) {
            correction = correction / 2;
        }
        return correction;
    };

    var updateCars = function(data) {
        for (var i = 0; i < data.data.length; i++) {
            var player = data.data[i];
            car = cars[player.id.name];

            if(car.id.name !== playerName) {
                updateOtherCar(player, car);
            } else {
                updateSituation(player, car);
            }
        }
    };

    var updateOtherCar = function(player, car) {
        var currentPiece = track.pieces[car.currentPieceIndex];
        var distanceDriven = getDistanceDriven(player,
                                                car.currentPieceDistance,
                                                player.piecePosition.inPieceDistance,
                                                car.currentPieceIndex === player.piecePosition.pieceIndex,
                                                car.currentPiece);

        car.lapDistance += Number.isNaN(distanceDriven) ? 0: distanceDriven;
        car.currentPieceIndex = player.piecePosition.pieceIndex;
        car.currentPieceDistance = player.piecePosition.inPieceDistance;

        car.updateSpeed(distanceDriven, tickDelta);

        if( otherCars[car.id.name].lapStarted ) {
            otherCars[car.id.name].lapStarted = false;
            //console.log(otherCars[car.id.name].lapSpeeds);
            otherCars[car.id.name].lapSpeeds[otherCars[car.id.name].lap] = [];
        }
        if( !Number.isNaN(car.speed)  ) {
            otherCars[car.id.name].lapSpeeds[otherCars[car.id.name].lap].push(
                { 
                    distance: (car.lapDistance * 10).toFixed(0), 
                    speed: car.speed,
                    lane: player.piecePosition.lane.endLaneIndex
                });
            otherCars[car.id.name].speed = car.speed;
        }
        //Util.log(otherCars[car.id.name]);
    };

    var getDistanceDriven = function(player, oldPosition, newPosition, samePiece, oldPiece) {
        if( samePiece ) {
            return player.piecePosition.inPieceDistance - car.currentPieceDistance;
        }
        else {
            return (oldPiece.length - oldPosition) + newPosition;
        }
    };

    // To use or not to use?
    var updateFrictionEstimate = function(radius, speed, tooMuch) {
        var friction = (speed * speed) / (gravity * Math.abs(radius));
        if( tooMuch ) {
            console.log("# Updating max friction", friction, frictionMax);
            frictionMax = Math.min(friction, frictionMax);
        } else if( !tooMuch ) {
            frictionMin = Math.max(friction, frictionMin);
        }
    };

    // To use or not to use?
    var getSpeedEstimate = function(radius) {
        return Math.sqrt(
                    (mass * gravity * frictionMin) /
                    (mass / Math.abs(radius)) );
    };

    var pieceSwitched = function(prevPieceIndex, currentPieceIndex, speed, lane) {
        Util.log("# Switched to piece " + car.currentPieceIndex + "/" + track.pieces.length);
        var prevPiece = getPiece(prevPieceIndex);
        var currentPiece = getPiece(currentPieceIndex);
        // Update the friction estimate based on the previous bend
        if( prevPiece.isBend && pieceMaxAngle < criticalAngle ) {
            updateFrictionEstimate(track.getBendRadius(prevPieceIndex, lane), pieceStartSpeed, false);
            /*
            var correction = getBinSearchCorrection();
            splineMaxSpeed += correction;
            console.log("# Spline max speed updated to " + splineMaxSpeed);
            */
        }
        pieceMaxAngle = -1;
        pieceStartSpeed = speed;
    };

    this.turboAvailable = function(data) {
        turboAvailable = true;
        console.log("# TurboAvailable");
    };

    var sendTurbo = function() {
        console.log("# TurboUsed");
	    turboAvailable = false;
        root.send({"msgType": "turbo", "data": "Risky businezzzzzzz!!!!"});
    };

    var updateSituation = function(player, car) {
        var currentPiece = track.pieces[car.currentPieceIndex];
        var distanceDriven = getDistanceDriven(player,
                                                car.currentPieceDistance,
                                                player.piecePosition.inPieceDistance,
                                                car.currentPieceIndex === player.piecePosition.pieceIndex,
                                                currentPiece);

        car.updateSpeed(distanceDriven, tickDelta);
        car.lapDistance += Number.isNaN(distanceDriven) ? 0: distanceDriven;

        var angleDelta = Math.abs(car.angle - player.angle);
        car.angle = player.angle;
        car.updateAngleSpeed(angleDelta, tickDelta);

        var nextPiece = track.pieces[car.currentPieceIndex+1];
        var startSpeed = 6.2;
        
        if(nextPiece && nextPiece.isSwitch){
            var targetLane = null;
            if( controlMode === ownSpeedMode ) {
                targetLane = Util.getShortestAvailableLane(car.currentPieceIndex, track);
            }
            else if ( carToFollow !== undefined ) {
                var c = otherCars[carToFollow];
                var lapData = c.lapSpeeds[c.fastestLap];
                var findLaneForDistance = function(array, distance) {
                    var lane = 0;
                    for( var i = 0; i < array.length - 1; ++i ) {
                        if( array[i].distance > distance + 500 ) {
                            lane = array[i].lane;
                            break;
                        }
                    }
                    //console.log("Using copied lane", lane);
                    return lane;
                }
                if( lapData !== undefined ) {
                    targetLane = findLaneForDistance(lapData, car.lapDistance);
                }
            }
            
            if(targetLane != null){
                if(targetLane < player.piecePosition.lane.endLaneIndex){
                    root.send({"msgType": "switchLane", "data": "Left"});
                }
                else if(targetLane > player.piecePosition.lane.endLaneIndex){
                    root.send({"msgType": "switchLane", "data": "Right"});
                }
            }
        }

        if( car.currentPieceIndex != player.piecePosition.pieceIndex ) {
            pieceSwitched(car.currentPieceIndex, player.piecePosition.pieceIndex, car.speed, car.currentLane);
            car.currentPiece = getPiece(player.piecePosition.pieceIndex);
            car.nextPiece = getPiece(player.piecePosition.pieceIndex+1);
        }

        car.currentPieceIndex = player.piecePosition.pieceIndex;
        car.currentPieceDistance = player.piecePosition.inPieceDistance;
        currentPiece = car.currentPieceIndex;

        if( player.piecePosition.lane.startLaneIndex != player.piecePosition.lane.endLaneIndex ) {
            car.isSwitchingLanes = true;
        } else if( car.isSwitchingLanes &&
                    player.piecePosition.lane.startLaneIndex ===
                    player.piecePosition.lane.endLaneIndex ) {
            car.isSwitchingLanes = false;
        }

        
        car.currentLane = player.piecePosition.lane.startLaneIndex;
        car.nextLane = player.piecePosition.lane.endLaneIndex;
        currentLane = car.currentLane;

        var lapPosition = track.getLapPosition(player.piecePosition.pieceIndex, player.piecePosition.inPieceDistance, player.piecePosition.lane.endLaneIndex);
        
        if( controlMode === ownSpeedMode ) {
            var splineSpeed = track.getSplineSpeed(lapPosition, car.currentLane, splineMaxSpeed, splineMinSpeed);
            car.setSteadySpeedOn(splineSpeed);
        }
        else if( controlMode === copyLapMode ) {
            car.copySpeedMode = 1;
            if( carToFollow === undefined || carToFollow === "") {
                var splineSpeed = track.getSplineSpeed(lapPosition, car.currentLane, splineMaxSpeed, splineMinSpeed);
                car.setSteadySpeedOn(splineSpeed);
            }
            else {
                var c = otherCars[carToFollow];
                var lapData = c.lapSpeeds[c.fastestLap];
                if( lapData === undefined ) {
                    controlMode = ownSpeedMode;
                    var splineSpeed = track.getSplineSpeed(lapPosition, car.currentLane, splineMaxSpeed, splineMinSpeed);
                    car.setSteadySpeedOn(splineSpeed);
                } else {
                    var findSpeedForDistance = function(array, distance) {
                        var speed = 4;
                        for( var i = 0; i < array.length - 1; ++i ) {
                            if( array[i].distance > distance ) {
                                speed = array[i].speed;
                                break;
                            }
                        }
                        return speed;
                    }
                    var speed = 3;
                    speed = Math.max(findSpeedForDistance(lapData, car.lapDistance), splineMinSpeed);
                    //console.log("Driving copied speed", speed);
                    car.setSteadySpeedOn(speed * 1.01);
                }
            }
        }
        // Last action to do: update car speed with each tick
        car.tick();
	//console.log("splineMaxSpeed", splineMaxSpeed.toFixed(2), "splineMinSpeed", splineMinSpeed.toFixed(2));	

        if( car.speed > 6 && car.throttle == 1 && turboAvailable ) {
            sendTurbo();
        }

        var radius = track.getBendRadius(car.currentPieceIndex, car.currentLane);
        save.updateData(car.currentPieceIndex, currentPiece.length, radius, car.speed, car.angle);
    };

    var getPiece = function(index){
        return track.pieces[index];
    };

};

module.exports = Brain;
