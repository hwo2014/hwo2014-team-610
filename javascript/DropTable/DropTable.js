var Brain = require('./Brain');

var DropTable = function(botName){

    var self = this;

    var startTime;
    var playerName = botName;

    var brain;

    this.init = function(data){
        brain = new Brain(data, playerName, this);
        brain.init();
    };

    this.start = function(data){
        startTime = new Date().getMilliseconds();
    };

    this.crash = function(data) {
        brain.crash(data);
    };

    this.turboAvailable = function(data) {
        brain.turboAvailable(data);
    };

    this.lapFinished = function(data) {
        brain.lapFinished(data);
    };

    this.tick = function(data) {
        brain.tick(data);
    };

    this.getThrottle = function() {
        return brain.getThrottle();
    };

    this.send = function(message) {
        client.write(JSON.stringify(message));
        return client.write('\n');
    };
};

module.exports = DropTable;
