var PID = function(p, i, d) {
    this.p = p;
    this.i = i;
    this.d = d;

    this.sumError  = 0;
    this.prevError = 0;
    this.prevTime  = 0;

    this.targetValue    = 0;
  

  this.setTarget = function(target) {
    this.targetValue = target;
  };

  this.update = function(current_value) {
    this.current_value = current_value;

    var error = (this.targetValue - this.current_value);
    this.sumError = this.sumError + error;
    var dError = error - this.prevError;
    this.prevError = error;
    return (this.p*error) + (this.i * this.sumError) + (this.d * dError);
  };
};

module.exports = PID;
