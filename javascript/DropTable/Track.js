var Util = require('./Util');
var Spline = require('../lib/Spline');

var Track = function(trackData) {
    var self = this;

    this.rawData = trackData;
    this.id = {};
    this.name = "";

    this.laps = -1;
    this.maxLapTimeMs = -1;
    this.quickRace = undefined;

    this.pieces = [];
    this.lanes = [];
    this.startingPoint = {};
    this.coordinates = [];
    this.splineSpeeds = [];

    this.laneTotals = [];

    this.init = function() {
        this.id = this.rawData.id;
        this.name = this.rawData.name;
        this.startingPoint = this.rawData.startingPoint;
        this.lanes = this.rawData.lanes;
        //Util.log(this.rawData.pieces);
        this.pieces = this.rawData.pieces.map(function(piece, index) {
            return {
                index: index,
                length: (piece.radius ? self.getBendLength(piece.radius, piece.angle): piece.length),
                laneLengths: (piece.radius ? self.getLaneLengths(piece.radius, piece.angle, self.lanes) : []),
                isBend: (piece.radius ? true: false),
                isSwitch: (piece.switch ? true: false),
                radius: (piece.radius !== undefined) ? piece.radius: -1,
                angle: (piece.angle !== undefined) ? piece.angle: -1
            };
        });
        //Util.log(this.pieces);
        for( var i = 0; i < this.lanes.length; ++i ){
            var coords = calcCoordinates(this.pieces, this.lanes[i]);
            this.coordinates.push(coords);
            this.splineSpeeds.push(normalizeSpline(populateSpline(coords)));
            this.laneTotals.push(calculateLaneTotal(this.lanes[i], this.pieces));
        }
        //Util.log(this.coordinates);
        //Util.log(this.splineSpeeds);
    };
    
    this.getLapPosition = function(pieceId, pieceDistance, lane) {
        var distanceDriven = 0;
        for( var i = 0; i < pieceId; ++i ) {
            var length = getPieceLengthWithLane(this.lanes[lane].distanceFromCenter, this.pieces[i]);
            distanceDriven += length;
        };
        distanceDriven += pieceDistance;
        return (distanceDriven / this.laneTotals[lane] * 100).toFixed(0);
    };

    var calculateLaneTotal = function( lane, pieces ) {
        var laneTotal = pieces.reduce(function(mem, piece) {
            return mem += getPieceLengthWithLane(lane.distanceFromCenter, piece);
        }, 0);
        return laneTotal;
    };

    var getPieceLengthWithLane = function(distanceFromCenter, piece) {
        if( !piece.isBend ) {
            return piece.length;
        }
        var laneRadius = piece.angle < 0 ? piece.radius + distanceFromCenter : piece.radius - distanceFromCenter;
        return self.getBendLength(laneRadius, piece.angle);
    }

    var populateSpline = function(coordinates) {
        var splineSpeed = function() {
            var prevX = 0;
            var prevY = 0;
            for( var i = 0; i < 1; i += 0.01 ) {
                var pos = spline.getPos(i); 
                var x = prevX - pos.left;
                var y = prevY - pos.top;
                var length = Math.sqrt((x*x)+(y*y));
                //OUTLOGconsole.log(i*100 + ',' + length);
                prevX = pos.left;
                prevY = pos.top;
            }
        };

        var points = coordinates.map(function(c) {
          return [c.coord[0], c.coord[1]];
        });
        var splineData = new Spline().buildSequence(points);
        var prevX = 0;
        var prevY = 0;
        var splineStep = 0.01;
        var res = [];
        for(var i = 0; i <= 1 / splineStep; ++i) {
            var pos = splineData.getPos(i*splineStep); 
            var x = prevX - pos.left;
            var y = prevY - pos.top;
            var length = Math.sqrt((x*x)+(y*y));
            res.push(length);
            prevX = pos.left;
            prevY = pos.top;
        }
        return res;
    };

    var normalizeSpline = function(splineData) {
        var maxFn = function(a,b) { return a > b ? a: b; };
        var minFn = function(a,b) { return a < b ? a: b; };
        
        // Experiment
        // Copy array
        var sortedValues = splineData.slice(0);
        sortedValues.sort(function(a,b) { return a-b; });
        //console.log("SortedValues", sortedValues);
        var maxCutIndex = (sortedValues.length * 0.8).toFixed(0);
        var cut = sortedValues[maxCutIndex];
        //console.log("Cut", cut, "CutIndex", cutIndex);
        var cutData = splineData.map(function(v) { return v > cut ? cut: v; });
        var max = cutData.reduce(maxFn, -999999999);
        var min = cutData.reduce(minFn, 999999999);
        var normalData = cutData.map( function(v) { return v - min; }).map(function(v) { return v / max; });

        return normalData;
    };

    this.getSplineSpeed = function(lapPercentage, laneId, maxSpeed, minSpeed) {
        if( lapPercentage === undefined || laneId === undefined || maxSpeed === undefined || minSpeed === undefined ) {
            return 0;
        }
        var speed = this.splineSpeeds[laneId][lapPercentage] * maxSpeed;
        return speed < minSpeed ? minSpeed: speed;
    };

    this.getBendRadius = function(pieceId, laneId) {
        if( !this.pieces[pieceId] || !this.pieces[pieceId].isBend ) {
            return -1;
        }
        var piece = this.pieces[pieceId];
        var laneDistance = this.lanes[laneId].distanceFromCenter;

        if(piece.angle > 0) laneDistance = laneDistance * -1;

        return piece.radius + laneDistance;
    };

    // sort by lane length in ascending order
    this.sortByLength = function(a, b){
        if (a.length < b.length){
            return -1;
        }
        if (a.length > b.length){
            return 1;
        }
        return 0;
    };
    
    this.getLaneLengths = function(radiusToCenter, angle, lanes){
        return lanes.map(function(lane, index) {
            var radius = angle < 0 ? radiusToCenter + lane.distanceFromCenter : radiusToCenter - lane.distanceFromCenter;

            return {
                index: index,
                length: self.getBendLength(radius, angle)
            };
        });
    };
    
    // This should ignore lanes
    this.getBendLength = function(radius, angle) {
        return Math.abs((angle/360) * (2*Math.PI*radius));
    };

    var calcCoordinates = function(pieces, lane) {
        // 13m, 22,6 degrees -> (12,4.9)
        var polarToCoordinates = function(length, angle) {
            return [length * Math.cos(angle * Math.PI / 180), length * Math.sin(angle * Math.PI / 180)];
        };
        var arcToCoordDelta = function(radius, curveAngle, angle) {
            var tempAngle = curveAngle / 2;
            var length = Math.abs(2 * ( Math.sin(tempAngle * Math.PI / 180) * radius ));
            return polarToCoordinates(length, angle + tempAngle);
        };

        var currentAngle = 0;
        var laneDiffAngle = lane.distanceFromCenter < 0 ? -90: 90;
        var straightLaneOffset = polarToCoordinates(Math.abs(lane.distanceFromCenter), currentAngle + laneDiffAngle);
        var currentCoord = straightLaneOffset;
        var zoomFactor = 1;
        var coordinates = pieces.map(function(piece) {
            var radius = piece.angle < 0 ? piece.radius + lane.distanceFromCenter : piece.radius - lane.distanceFromCenter;
            var coordDelta = 0;
            if( piece.isBend ) {
                coordDelta = arcToCoordDelta(radius, piece.angle, currentAngle);
            }
            else {
                var baseCoord = polarToCoordinates(piece.length, currentAngle);
                coordDelta = [baseCoord[0], baseCoord[1]];
            }
            currentCoord = [zoomFactor * (currentCoord[0]+coordDelta[0]), zoomFactor * (currentCoord[1]+coordDelta[1])];
            currentAngle = piece.isBend ?
                            currentAngle + piece.angle:
                            currentAngle;
            return { id: piece.index, coord: currentCoord, isBend: piece.isBend, angle: currentAngle, radius: radius };
        });
        return coordinates;
    };
};

module.exports = Track;
