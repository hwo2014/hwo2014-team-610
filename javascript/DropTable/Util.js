var Util = {
	log: function(data) {
		console.log(JSON.stringify(data, null, 2));
	},
    // TODO: check if there is a car in front of us on the lane.
    getShortestAvailableLane: function(currentIndex, track){
        var bendPieces = this.getBendPiecesBeforeNextSwitch(currentIndex, track);
        var minLane = null;
        var minLaneLength = -1;
        if(bendPieces.length > 0){
            for(var i = 0; i < bendPieces[0].laneLengths.length; ++i){
                var laneLength = 0;
                for(var b = 0; b < bendPieces.length; ++b){
                    laneLength += bendPieces[b].laneLengths[i].length;
                }
                if(minLaneLength == -1 || minLaneLength > laneLength){
                    minLane = bendPieces[0].laneLengths[i].index;
                    minLaneLength = laneLength;
                }
            }
        }
	   
	   return minLane;
    },
    getBendPiecesBeforeNextSwitch: function(currentIndex, track){
        var i = currentIndex + 2; // The currentIndex + 1 is a switch.
        var bendPieces = [];
        // If we got to the piece we started from, break.
        while(i != currentIndex+1){
            // If we got to the last piece on the track, contnue from the first piece of the track.
            if(i >= track.pieces.length){
				i = 0;
			}
            
            // If we got to the next switch piece.
            if(track.pieces[i].isSwitch){
                break;
            }
			// If the track piece is a bend.
            if(track.pieces[i].isBend){
				bendPieces.push(track.pieces[i]);
			}
			++i;
        }
        return bendPieces;
    }
};

module.exports = Util;
