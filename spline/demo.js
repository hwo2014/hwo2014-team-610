// Generate a random example spline and fancy visualization to help see what the plugin is doing
DEMO = {
	showWaypoints: false,
	showTrail: true
	};

DEMO.run = function() {

	var numPoints = 40;
	var dotsPerSeg = 2;
	var i;

	var points = [];

	var keimola = 
  [
    {
      "id": 0,
      "coord": [
        102,
        -10
      ],
      "isBend": false,
      "angle": 0,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 1,
      "coord": [
        196,
        -10
      ],
      "isBend": false,
      "angle": 0,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 2,
      "coord": [
        276.3635207966688,
        5.985298172629777
      ],
      "isBend": true,
      "angle": 22.5,
      "radius": 210,
      "laneDistance": -10
    },
    {
      "id": 3,
      "coord": [
        344.49242404917493,
        51.50757595082501
      ],
      "isBend": true,
      "angle": 45,
      "radius": 210,
      "laneDistance": -10
    },
    {
      "id": 4,
      "coord": [
        390.0147018273702,
        119.63647920333113
      ],
      "isBend": true,
      "angle": 67.5,
      "radius": 210,
      "laneDistance": -10
    },
    {
      "id": 5,
      "coord": [
        405.99999999999994,
        199.99999999999997
      ],
      "isBend": true,
      "angle": 90,
      "radius": 210,
      "laneDistance": -10
    },
    {
      "id": 6,
      "coord": [
        373.7817459305202,
        277.7817459305202
      ],
      "isBend": true,
      "angle": 135,
      "radius": 110,
      "laneDistance": -10
    },
    {
      "id": 7,
      "coord": [
        295.99999999999994,
        309.99999999999994
      ],
      "isBend": true,
      "angle": 180,
      "radius": 110,
      "laneDistance": -10
    },
    {
      "id": 8,
      "coord": [
        242.99999999999994,
        309.99999999999994
      ],
      "isBend": false,
      "angle": 180,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 9,
      "coord": [
        170.2901478506329,
        324.46288882285546
      ],
      "isBend": true,
      "angle": 157.5,
      "radius": 190,
      "laneDistance": -10
    },
    {
      "id": 10,
      "coord": [
        148.64230384478503,
        346.11073282870336
      ],
      "isBend": true,
      "angle": 112.5,
      "radius": 40,
      "laneDistance": -10
    },
    {
      "id": 11,
      "coord": [
        89.11073282870336,
        405.64230384478503
      ],
      "isBend": true,
      "angle": 157.5,
      "radius": 110,
      "laneDistance": -10
    },
    {
      "id": 12,
      "coord": [
        4.9203777083836115,
        405.64230384478503
      ],
      "isBend": true,
      "angle": 202.5,
      "radius": 110,
      "laneDistance": -10
    },
    {
      "id": 13,
      "coord": [
        -54.61119330769807,
        346.11073282870336
      ],
      "isBend": true,
      "angle": 247.5,
      "radius": 110,
      "laneDistance": -10
    },
    {
      "id": 14,
      "coord": [
        -76.25903731354596,
        324.46288882285546
      ],
      "isBend": true,
      "angle": 202.5,
      "radius": 40,
      "laneDistance": -10
    },
    {
      "id": 15,
      "coord": [
        -106.87371190275314,
        324.46288882285546
      ],
      "isBend": true,
      "angle": 157.5,
      "radius": 40,
      "laneDistance": -10
    },
    {
      "id": 16,
      "coord": [
        -128.52155590860102,
        346.11073282870336
      ],
      "isBend": true,
      "angle": 112.5,
      "radius": 40,
      "laneDistance": -10
    },
    {
      "id": 17,
      "coord": [
        -142.9844447314565,
        418.8205849780704
      ],
      "isBend": true,
      "angle": 90,
      "radius": 190,
      "laneDistance": -10
    },
    {
      "id": 18,
      "coord": [
        -142.9844447314565,
        512.8205849780704
      ],
      "isBend": false,
      "angle": 90,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 19,
      "coord": [
        -142.9844447314565,
        606.8205849780704
      ],
      "isBend": false,
      "angle": 90,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 20,
      "coord": [
        -160.55803786026365,
        649.2469918492633
      ],
      "isBend": true,
      "angle": 135,
      "radius": 60,
      "laneDistance": -10
    },
    {
      "id": 21,
      "coord": [
        -202.9844447314565,
        666.8205849780704
      ],
      "isBend": true,
      "angle": 180,
      "radius": 60,
      "laneDistance": -10
    },
    {
      "id": 22,
      "coord": [
        -245.41085160264936,
        649.2469918492633
      ],
      "isBend": true,
      "angle": 225,
      "radius": 60,
      "laneDistance": -10
    },
    {
      "id": 23,
      "coord": [
        -262.9844447314565,
        606.8205849780704
      ],
      "isBend": true,
      "angle": 270,
      "radius": 60,
      "laneDistance": -10
    },
    {
      "id": 24,
      "coord": [
        -262.9844447314565,
        507.8205849780704
      ],
      "isBend": false,
      "angle": 270,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 25,
      "coord": [
        -262.9844447314565,
        408.8205849780704
      ],
      "isBend": false,
      "angle": 270,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 26,
      "coord": [
        -289.3448344246672,
        345.18097467128115
      ],
      "isBend": true,
      "angle": 225,
      "radius": 90,
      "laneDistance": -10
    },
    {
      "id": 27,
      "coord": [
        -352.9844447314565,
        318.8205849780705
      ],
      "isBend": true,
      "angle": 180,
      "radius": 90,
      "laneDistance": -10
    },
    {
      "id": 28,
      "coord": [
        -416.6240550382458,
        345.1809746712812
      ],
      "isBend": true,
      "angle": 135,
      "radius": 90,
      "laneDistance": -10
    },
    {
      "id": 29,
      "coord": [
        -494.405800968766,
        377.399228740761
      ],
      "isBend": true,
      "angle": 180,
      "radius": 110,
      "laneDistance": -10
    },
    {
      "id": 30,
      "coord": [
        -574.7693217654348,
        361.4139305681312
      ],
      "isBend": true,
      "angle": 202.5,
      "radius": 210,
      "laneDistance": -10
    },
    {
      "id": 31,
      "coord": [
        -642.898225017941,
        315.89165278993596
      ],
      "isBend": true,
      "angle": 225,
      "radius": 210,
      "laneDistance": -10
    },
    {
      "id": 32,
      "coord": [
        -680.3748844208279,
        278.4149933870489
      ],
      "isBend": false,
      "angle": 225,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 33,
      "coord": [
        -725.8971621990232,
        210.28609013454286
      ],
      "isBend": true,
      "angle": 247.5,
      "radius": 210,
      "laneDistance": -10
    },
    {
      "id": 34,
      "coord": [
        -741.882460371653,
        129.92256933787402
      ],
      "isBend": true,
      "angle": 270,
      "radius": 210,
      "laneDistance": -10
    },
    {
      "id": 35,
      "coord": [
        -709.6642063021732,
        52.14082340735381
      ],
      "isBend": true,
      "angle": 315,
      "radius": 110,
      "laneDistance": -10
    },
    {
      "id": 36,
      "coord": [
        -641.535303049667,
        6.618545629158575
      ],
      "isBend": true,
      "angle": 337.5,
      "radius": 210,
      "laneDistance": -10
    },
    {
      "id": 37,
      "coord": [
        -561.1717822529982,
        -9.366752543471241
      ],
      "isBend": true,
      "angle": 360,
      "radius": 210,
      "laneDistance": -10
    },
    {
      "id": 38,
      "coord": [
        -467.17178225299824,
        -9.366752543471264
      ],
      "isBend": false,
      "angle": 360,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 39,
      "coord": [
        -373.17178225299824,
        -9.366752543471287
      ],
      "isBend": false,
      "angle": 360,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 40,
      "coord": [
        -279.17178225299824,
        -9.36675254347131
      ],
      "isBend": false,
      "angle": 360,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 41,
      "coord": [
        -185.17178225299824,
        -9.366752543471334
      ],
      "isBend": false,
      "angle": 360,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 42,
      "coord": [
        -91.17178225299824,
        -9.366752543471357
      ],
      "isBend": false,
      "angle": 360,
      "radius": -11,
      "laneDistance": -10
    },
    {
      "id": 43,
      "coord": [
        -0.17178225299824135,
        -9.36675254347138
      ],
      "isBend": false,
      "angle": 360,
      "radius": -11,
      "laneDistance": -10
    }
  ];

	var xOffset = 800;
    var yOffset = 120;
    keimola.forEach(function(c) {
      points.push([c.coord[0]+xOffset, c.coord[1]+yOffset]);
    });

/*
	// Make a random list of waypoints for the animation to follow
	for (i=0; i<numPoints; i++) {
		points.push([Math.floor(Math.random()*(maxX-minX))+minX, Math.floor(Math.random()*(maxY-minY))+minY]);
	}
	*/

	// -- Important bit #1: Generate the spline animation object --
	var spline = $.crSpline.buildSequence(points);
	
	// Clean up visuals if we've run this once already
	$("#mover").remove();
	$(".waypoint").remove();
	$(".path-dot").remove();

	// Scary-looking stuff to visualize the waypoints and the trail of dots
	// NOT needed for animation
	for (i=0; i<points.length; i++) {
		$('<div class="waypoint">' + i + '</div>')
			.appendTo($(document.body))
			.css({
				left: points[i][0],
				top: points[i][1],
				display: (DEMO.showWaypoints ? "inline" : "none")
			});

		for (var j=0; j<dotsPerSeg; j++) {
			var t = (i + j/dotsPerSeg) / points.length;
			var pos = spline.getPos(t);
			$('<div class="path-dot" />')
				.appendTo($(document.body))
				.css({
					left: pos.left,
					top: pos.top,
					display: (DEMO.showTrail ? "inline" : "none")
				});
		}
	}

	// -- Important bit #2: Actually animate our mover object. --
	$('<div id="mover" />')
		.appendTo($(document.body))
		.animate({ crSpline: spline }, 20000, function () {
			// Re-run the demo with a new spline after we're done
			window.setTimeout(function() {
				DEMO.run();
			}, 500);
		});
	
};

$(document).ready(function() {
	$("#show-trail").click(function () {
		if ($(this).is(":checked")) {
			$(".path-dot").css({display: "inline"});
			DEMO.showTrail = true;
		}
		else {
			$(".path-dot").css({display: "none"});
			DEMO.showTrail = false;
		}
	});

	$("#show-waypoints").click(function () {
		if ($(this).is(":checked")) {
			$(".waypoint").css({display: "inline"});
			DEMO.showWaypoints = true;
		}
		else {
			$(".waypoint").css({display: "none"});
			DEMO.showWaypoints = false;
		}
	});

	$("#show-trail").attr("checked", DEMO.showTrail);
	$("#show-waypoints").attr("checked", DEMO.showWaypoints);

	DEMO.run();
});
